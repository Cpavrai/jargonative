import React from 'react';
import UserContextType from '../models/UserContextType';

export const UserContextInitalState: UserContextType = {
  user: {
    username: '',
    picture: '',
    boards: [],
  },
  setUser: () => {},
};

export const UserContext = React.createContext(UserContextInitalState);
