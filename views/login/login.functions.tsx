import { Alert } from 'react-native';
import County from '../../database/db';

export default {
  login: (navigate: Function, setLoading: Function, data: Object, setUser: Function) => {
    setLoading(true);
    County.login(data.userName, data.userPassword)
      .then((user: any) => {
        County.getBoards()
          .then(boardList => {
            setUser({ username: user.username, picture: user.picture, boards: boardList });
            setLoading(false);
            navigate('Client');
          })
          .catch(err => {
            setLoading(false);
            console.log(err);
          });
      })
      .catch(err => {
        setLoading(false);
        Alert.alert(err.toString());
      });
  },
  goToRegister: (navigate: Function) => {
    navigate('Register');
  },
};
