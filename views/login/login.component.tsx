import { useHeaderHeight } from '@react-navigation/stack';
import { LinearGradient } from 'expo-linear-gradient';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { ActivityIndicator, Keyboard, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import { Button } from 'react-native-elements';
import { Linears } from '../../assets/colors/themes';
import { UserContext } from '../../context/user.context';
import loginFunctions from './login.functions';
import styles from './login.style';

const LoginComponent = ({ navigation: { navigate } }) => {
  const [userName, setUserName] = useState('test@test.com');
  const [userPassword, setUserPassword] = useState('azerty');
  const [loading, setLoading] = useState(false);

  const colorScheme = useColorScheme();
  useHeaderHeight();

  let passwordInput: TextInput;
  const loginFunction = loginFunctions;

  return (
    <UserContext.Consumer>
      {userData => (
        <LinearGradient
          colors={Array.from(Linears[colorScheme] || ['#dedede', '#dedede'])}
          style={{ flex: 1 }}
        >
          <TouchableOpacity
            style={styles.body}
            onPress={() => Keyboard.dismiss()}
            activeOpacity={1}
          >
            <View style={styles.bodyInputs}>
              <TextInput
                style={styles.input}
                placeholderTextColor="#aaa"
                placeholder="Identifiant"
                returnKeyType="next"
                selectionColor="black"
                value={userName}
                clearButtonMode="while-editing"
                onSubmitEditing={() => passwordInput.focus()}
                onChangeText={text => {
                  setUserName(text);
                }}
                autoCapitalize="none"
                enablesReturnKeyAutomatically
              />
              <TextInput
                style={styles.input}
                placeholderTextColor="#aaa"
                placeholder="Mot de passe"
                returnKeyType="go"
                selectionColor="black"
                textContentType="password"
                secureTextEntry
                editable
                maxLength={40}
                value={userPassword}
                clearButtonMode="while-editing"
                onSubmitEditing={() =>
                  loginFunction.login(
                    navigate,
                    setLoading,
                    {
                      userName,
                      userPassword,
                    },
                    userData.setUser
                  )
                }
                onChangeText={text => {
                  setUserPassword(text);
                }}
                enablesReturnKeyAutomatically
                ref={input => {
                  passwordInput = input;
                }}
              />
              <Button
                title="Se connecter"
                buttonStyle={styles.button}
                disabled={userName === '' || userPassword === ''}
                onPress={() =>
                  loginFunction.login(
                    navigate,
                    setLoading,
                    {
                      userName,
                      userPassword,
                    },
                    userData.setUser
                  )
                }
              />

              <TouchableOpacity onPress={() => loginFunction.goToRegister(navigate)}>
                <Text style={styles.link}>Pas encore de compte ?</Text>
              </TouchableOpacity>
            </View>
            {loading && <ActivityIndicator size="large" color="#ffffff" style={styles.loader} />}
          </TouchableOpacity>
        </LinearGradient>
      )}
    </UserContext.Consumer>
  );
};

LoginComponent.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default LoginComponent;
