import React, { useState } from 'react';
import { Image, Text, View } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Fonts } from '../../../../assets/colors/themes';
import { UserContext } from '../../../../context/user.context';
import ProfileFunction from './profile.function';
import styles from './profile.style';

const DEFAULT_PICTURE = require('../../../../assets/pictures/user_circle.png');

const ProfileComponent = () => {
  const profileFunction = ProfileFunction;
  const colorScheme = useColorScheme();
  const [updatedPicture, setUpdatedPicture] = useState(0);

  return (
    <UserContext.Consumer>
      {({ user, setUser }) => (
        <View style={styles.body}>
          <Text style={{ ...styles.header, color: Fonts[colorScheme] }}>
            Bonjour {user.username} ! Ici s&apos;affiche votre profil.
          </Text>
          <TouchableOpacity
            onPress={() => {
              profileFunction.changeProfilePicture(setUser, () =>
                setUpdatedPicture(updatedPicture + 1)
              );
            }}
            style={styles.pictureButton}
          >
            <Image
              source={user.picture ? { uri: user.picture } : DEFAULT_PICTURE}
              style={styles.picture}
            />
          </TouchableOpacity>
        </View>
      )}
    </UserContext.Consumer>
  );
};

export default ProfileComponent;
