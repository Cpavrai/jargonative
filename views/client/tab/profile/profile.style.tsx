import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  body: { padding: '10%', flex: 1 },
  header: { alignItems: 'center', marginBottom: 50, alignSelf: 'center' },
  pictureButton: {
    alignSelf: 'center',
  },
  picture: {
    width: 150,
    height: 150,
    borderRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.8)',
    borderWidth: 10,
  },
});

export default styles;
