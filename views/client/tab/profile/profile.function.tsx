import * as ImagePicker from 'expo-image-picker';

export default {
  changeProfilePicture: async (setUser: Function, increaseUpdatedPicture: Function) => {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      aspect: [4, 3],
      quality: 1,
    });
    if (result.cancelled === false) {
      setUser({ picture: result.uri });
      increaseUpdatedPicture();
    }
    // ImagePicker.requestCameraPermissionsAsync().then(res => {
    //   if (res.granted === true) {
    //     console.log("Donc c'est bon");
    //     // ImagePicker.launchImageLibraryAsync({}).then(res => console.log(res));
    //   } else {
    //     Alert.alert('Vous ne pourrez pas changer votre photo de profil');
    //   }
    // });
  },
};
