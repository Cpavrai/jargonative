import HomeComponent from './home';
import ProfileComponent from './profile';
import SettingsComponent from './settings';

export { HomeComponent, ProfileComponent, SettingsComponent };
