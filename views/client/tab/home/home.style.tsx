import { StyleSheet, PixelRatio } from 'react-native';

const styles = StyleSheet.create({
  body: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  bodyInputs: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    marginTop: PixelRatio.get() * 5,
    backgroundColor: '#dedede',
    padding: 20,
    color: 'black',
    width: '50%',
  },
  button: {
    backgroundColor: '#78e08f',
    marginTop: PixelRatio.get() * 10,
    paddingHorizontal: PixelRatio.get() * 10,
  },
});

export default styles;
