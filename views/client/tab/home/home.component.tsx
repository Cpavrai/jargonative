import React from 'react';
import { Text, View } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import { Fonts } from '../../../../assets/colors/themes';
import { UserContext } from '../../../../context/user.context';

const HomeComponent = () => {
  const colorScheme = useColorScheme();

  return (
    <UserContext.Consumer>
      {userData => (
        <View style={{ padding: '10%', flex: 1 }}>
          <Text style={{ color: Fonts[colorScheme], alignSelf: 'center' }}>
            Bonjour {userData.user.username} ! Tu trouveras ici ton accueil
          </Text>
          {userData.user.boards.map(board => {
            return (
              <Text
                key={board.id}
                style={{
                  color: Fonts[colorScheme],
                  alignSelf: 'center',
                  marginVertical: 10,
                  backgroundColor: 'grey',
                }}
              >
                {board.data.title}, {board.data.description}
              </Text>
            );
          })}
        </View>
      )}
    </UserContext.Consumer>
  );
};

export default HomeComponent;
