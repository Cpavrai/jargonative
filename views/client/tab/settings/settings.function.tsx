import { Alert } from 'react-native';

const ALERT_MESSAGE = "Cette fonctionnalité n'est pas encore sortie. Reviens plus tard 😳";

export default {
  changeUsername: () => {
    Alert.alert(ALERT_MESSAGE);
  },
  changePassWord: () => {
    Alert.alert(ALERT_MESSAGE);
  },
};
