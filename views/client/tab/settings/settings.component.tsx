import { FontAwesome5 } from '@expo/vector-icons';
import React from 'react';
import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { UserContext } from '../../../../context/user.context';
import SettingsFunction from './settings.function';
import styles from './settings.style';

const SettingsComponent = () => {
  const settingsFunction = SettingsFunction;

  return (
    <UserContext.Consumer>
      {() => (
        <View style={styles.body}>
          <TouchableOpacity onPress={() => settingsFunction.changeUsername()} style={styles.button}>
            <Text style={{ justifyContent: 'center' }}>
              <FontAwesome5 name="user-tag" style={{ color: 'black' }} /> Changer le pseudo
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => settingsFunction.changePassWord()} style={styles.button}>
            <Text style={{ justifyContent: 'center' }}>
              <FontAwesome5 name="key" style={{ color: 'black' }} /> Changer le mot de passe
            </Text>
          </TouchableOpacity>
        </View>
      )}
    </UserContext.Consumer>
  );
};

export default SettingsComponent;
