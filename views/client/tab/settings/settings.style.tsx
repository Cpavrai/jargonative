import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  body: { padding: '10%', justifyContent: 'center', flex: 1 },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
});

export default styles;
