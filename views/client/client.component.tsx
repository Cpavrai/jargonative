import React, { useContext } from 'react';
import { Dimensions, StatusBar } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import { SceneMap, TabBar, TabView } from 'react-native-tab-view';
import { Sets } from '../../assets/colors/themes';
import { HomeComponent, ProfileComponent, SettingsComponent } from './tab';
import { UserContext } from '../../context/user.context';

const ClientComponent = () => {
  const colorScheme = useColorScheme();

  const [index, setIndex] = React.useState(0);
  const initialLayout = {
    height: 0,
    width: Dimensions.get('window').width,
  };
  const [routes] = React.useState([
    { key: 'home', title: 'Accueil' },
    { key: 'profile', title: 'Profil' },
    { key: 'settings', title: 'Paramètres' },
  ]);

  const renderScene = SceneMap({
    home: HomeComponent,
    profile: ProfileComponent,
    settings: SettingsComponent,
  });
  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: Sets[colorScheme].indicator }}
      style={{ backgroundColor: Sets[colorScheme].bar }}
    />
  );

  StatusBar.setBarStyle('dark-content', true);

  useContext(UserContext);

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      renderTabBar={renderTabBar}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
      style={{ backgroundColor: Sets[colorScheme].background }}
    />
  );
};

export default ClientComponent;
