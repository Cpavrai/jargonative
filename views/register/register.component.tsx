import { useHeaderHeight } from '@react-navigation/stack';
import { LinearGradient } from 'expo-linear-gradient';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Keyboard, TextInput, TouchableOpacity, View } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import { Button } from 'react-native-elements';
import { Linears } from '../../assets/colors/themes';
import { UserContext } from '../../context/user.context';
import registerFunctions from './register.functions';
import styles from './register.style';

const RegisterComponent = ({ navigation: { navigate } }) => {
  const [userMail, setUserMail] = useState('');
  const [userId, setUserId] = useState('');
  const [userPassword, setUserPassword] = useState('');
  const [validUserMail, setValidUserMail] = useState(true);
  const colorScheme = useColorScheme();
  let passwordInput: TextInput;
  let mailInput: TextInput;
  const registerFunction = registerFunctions;
  useHeaderHeight();

  return (
    <UserContext.Consumer>
      {({ setUser }) => (
        <LinearGradient
          colors={Array.from(Linears[colorScheme] || ['#dedede', '#dedede'])}
          style={{ flex: 1 }}
        >
          <TouchableOpacity
            style={styles.body}
            onPress={() => Keyboard.dismiss()}
            activeOpacity={1}
          >
            <View style={styles.bodyInputs}>
              <TextInput
                style={styles.input}
                placeholderTextColor="#aaa"
                placeholder="Identifiant"
                returnKeyType="next"
                selectionColor="black"
                value={userId}
                clearButtonMode="while-editing"
                onSubmitEditing={() => mailInput.focus()}
                onChangeText={text => {
                  setUserId(text);
                }}
                enablesReturnKeyAutomatically
              />
              <TextInput
                style={
                  validUserMail ? { ...styles.input } : { ...styles.invalidInput, ...styles.input }
                }
                placeholderTextColor="#aaa"
                placeholder="Adresse email"
                returnKeyType="next"
                selectionColor="black"
                value={userMail}
                clearButtonMode="while-editing"
                onSubmitEditing={() => passwordInput.focus()}
                onChangeText={text => {
                  registerFunction.checkEmail(text, setValidUserMail);
                  setUserMail(text);
                }}
                enablesReturnKeyAutomatically
                ref={input => {
                  mailInput = input;
                }}
              />
              <TextInput
                style={styles.input}
                placeholderTextColor="#aaa"
                placeholder="Mot de passe"
                returnKeyType="go"
                selectionColor="black"
                textContentType="password"
                secureTextEntry
                editable
                maxLength={40}
                value={userPassword}
                clearButtonMode="while-editing"
                onSubmitEditing={() =>
                  registerFunction.register(
                    navigate,
                    {
                      userId,
                      userMail,
                      userPassword,
                    },
                    setUser
                  )
                }
                onChangeText={text => {
                  setUserPassword(text);
                }}
                enablesReturnKeyAutomatically
                ref={input => {
                  passwordInput = input;
                }}
              />
              <Button
                title="S'enregistrer"
                buttonStyle={styles.button}
                disabled={userId === '' || userMail === '' || !validUserMail || userPassword === ''}
                onPress={() =>
                  registerFunction.register(
                    navigate,
                    {
                      userId,
                      userMail,
                      userPassword,
                    },
                    setUser
                  )
                }
              />
            </View>
          </TouchableOpacity>
        </LinearGradient>
      )}
    </UserContext.Consumer>
  );
};

RegisterComponent.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default RegisterComponent;
