import { Alert } from 'react-native';

export default {
  // eslint-disable-next-line
  register: (navigate: Function, data: Object, setUser: Function) => {
    Alert.alert('Pas encore implémenté');
    // setUser({ username: data.userName });
    // navigate('Client');
  },
  checkEmail: (text: string, setValidUserMail: Function) => {
    const reg = new RegExp(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/);
    setValidUserMail(reg.test(text));
  },
};
