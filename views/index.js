import ClientComponent from './client';
import LoginComponent from './login';
import RegisterComponent from './register';

export { ClientComponent, LoginComponent, RegisterComponent };
