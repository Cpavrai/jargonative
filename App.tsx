import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { View } from 'react-native';
import { AppearanceProvider } from 'react-native-appearance';
import styles from './App.style';
import { ClientComponent, LoginComponent, RegisterComponent } from './views';
import InitializeFirebase from './database/initialize';
import { UserContext, UserContextInitalState } from './context/user.context';

export default function App() {
  const Stack = createStackNavigator();
  InitializeFirebase();
  const ucis = UserContextInitalState;
  ucis.setUser = data => {
    ucis.user = { ...ucis.user, ...data };
  };

  return (
    <UserContext.Provider value={ucis}>
      <AppearanceProvider>
        <View style={styles.container}>
          <NavigationContainer>
            <Stack.Navigator initialRouteName="Login">
              <Stack.Screen
                name="Login"
                component={LoginComponent}
                options={{
                  title: 'Se connecter',
                  headerTransparent: true,
                  headerTitleStyle: {
                    color: 'white',
                  },
                }}
              />
              <Stack.Screen
                name="Register"
                component={RegisterComponent}
                options={{
                  title: 'Rejoindre',
                  headerTransparent: true,
                  headerTintColor: 'white',
                  headerTitleStyle: {
                    color: 'white',
                  },
                }}
              />
              <Stack.Screen
                name="Client"
                component={ClientComponent}
                options={{
                  headerBackTitle: 'Logout',
                }}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </AppearanceProvider>
    </UserContext.Provider>
  );
}
