export interface BoardData {
  title: string;
  description: string;
}

export interface UserBoardsType {
  id: string;
  data: BoardData;
}

export default UserBoardsType;
