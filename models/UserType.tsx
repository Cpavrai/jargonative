// eslint-disable-next-line
import UserBoardsType from './Boards';

export interface UserType {
  username: string;
  picture: string;
  boards: Array<UserBoardsType>;
}

export default UserType;
