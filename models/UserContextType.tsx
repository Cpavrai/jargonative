import UserType from './UserType';

export interface UserContextType {
  user: UserType;
  setUser: Function;
}

export default UserContextType;
