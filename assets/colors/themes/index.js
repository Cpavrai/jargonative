import LightTheme from './light.theme';
import DarkTheme from './dark.theme';

// Fonts colors for each color scheme
const Fonts = {
  light: LightTheme.font,
  dark: DarkTheme.font,
  'no-preference': LightTheme.font,
};

// Sets of colors for each color scheme
const Sets = {
  light: {
    indicator: LightTheme.linear[0],
    bar: LightTheme.linear[LightTheme.linear.length - 1],
    background: LightTheme.background,
  },
  dark: {
    indicator: DarkTheme.linear[0],
    bar: DarkTheme.linear[DarkTheme.linear.length - 1],
    background: DarkTheme.background,
  },
  'no-preference': {
    indicator: LightTheme.linear[0],
    bar: LightTheme.linear[LightTheme.linear.length - 1],
    background: LightTheme.background,
  },
};

// Linears for each color scheme
const Linears = {
  light: LightTheme.linear,
  dark: DarkTheme.linear,
  'no-preference': LightTheme.linear,
};

export { LightTheme, DarkTheme, Fonts, Sets, Linears };
