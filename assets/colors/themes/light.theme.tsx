import Theme from './theme.interface';

const LightTheme: Theme = {
  background: 'white',
  font: 'black',
  linear: ['#a6c1ee', '#fbc2eb'],
};

export default LightTheme;
