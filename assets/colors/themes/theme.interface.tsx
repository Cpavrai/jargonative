export interface Theme {
  background: string;
  font: string;
  linear: Array<string>;
}

export default Theme;
