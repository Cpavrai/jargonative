import Theme from './theme.interface';

const DarkTheme: Theme = {
  background: '#093b6d',
  font: 'white',
  linear: ['#4c669f', '#3b5998', '#192f6a'],
};

export default DarkTheme;
