import '@firebase/firestore';
import { decode as atob, encode as btoa } from 'base-64';
import firebase from 'firebase';
import AuthService from './authService';

const globalAny: any = global;

globalAny.btoa = btoa;
globalAny.atob = atob;

interface Board {
  description: string;
  currency: string;
  title: string;
  users: Array<string>;
}

export default class County {
  static login(email: string, password: string) {
    return new Promise((resolve, reject) => {
      AuthService.signInUser(email, password).then(
        () => {
          firebase
            .firestore()
            .collection('users')
            .doc(firebase.auth().currentUser.uid)
            .get()
            .then(res => {
              AuthService.username = res.data().username;
              resolve(res.data());
            });
        },
        err => {
          reject(err);
        }
      );
    });
  }

  static getBoards() {
    const userId = firebase.auth().currentUser.uid;

    return new Promise((resolve, reject) => {
      firebase
        .firestore()
        .collection('users')
        .doc(userId)
        .collection('boards')
        .get()
        .then(res => {
          const tab = [];
          res.forEach(r => {
            tab.push({ id: r.id, data: r.data() });
          });
          resolve(tab);
        })
        .catch(err => reject(err));
    });
  }

  static addBoard(title: string, description: string, currency: string, users: Array<string>) {
    users.unshift(AuthService.username);

    const data: Board = {
      title,
      description,
      currency,
      users,
    };

    const dbh = firebase.firestore();

    dbh
      .collection('boards')
      .add(data)
      .then(board => {
        users.forEach(user => {
          this.addBoardToUser(data, board.id, user);
        });
      });
  }

  static addBoardToUser(boardData: Board, boardID: string, username: string) {
    const board = {
      title: boardData.title,
      description: boardData.description,
    };

    firebase
      .firestore()
      .collection('users')
      .where('username', '==', username)
      .get()
      .then(res => {
        res.forEach(doc => {
          doc.ref
            .collection('boards')
            .doc(boardID)
            .set(board)
            .then();
        });
      });
  }

  static addExpense(boardId, title, amount, date, paidBy) {
    const data = {
      title,
      amount,
      date: new Date(date),
      paidBy,
    };

    firebase
      .firestore()
      .collection('boards')
      .doc(boardId)
      .collection('expenses')
      .add(data)
      .then(res => {
        console.log('ok', res);
      });
  }
}
