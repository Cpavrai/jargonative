import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyDP_mJBbh7X6H79WFcZ4z_1bkbJiEX9iQ4',
  authDomain: 'county-efcd5.firebaseapp.com',
  databaseURL: 'https://county-efcd5.firebaseio.com/',
  projectId: 'county-efcd5',
  storageBucket: 'county-efcd5.appspot.com',
  messagingSenderId: '145250389130',
  appId: '1:145250389130:web:952cd2dfb8f72eaf0db173',
};

const InitializeFirebase = () => {
  firebase.initializeApp(firebaseConfig);
};

export default InitializeFirebase;
