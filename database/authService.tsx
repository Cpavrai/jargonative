import firebase from 'firebase';

export default class AuthService {
  static isAuth = false;

  static username = '';

  // constructor() {
  //     firebase.auth().onAuthStateChanged((user) => {
  //         if (user) {
  //             this.isAuth = true;
  //         } else {
  //             this.isAuth = false;
  //         }
  //     });
  // }

  static signUpUser(email: string, password: string) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(
          user => {
            resolve(user);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  static signInUser(email: string, password: string) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(
          user => {
            resolve(user);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  static signOut() {
    firebase.auth().signOut();
  }
}
