# JargoNative

## What is JargoNative ?

This project is about a mobile application, enabled in every platform (Android and iOS), which permits you to see your last group transactions and cash-share.

## How is it built ?

This project is structured as several parts :

| Name | Path | Description |
| -- | -- | -- |
| Assets | ./assets | Folder containing every no-code content, as pictures or videos. |
| External Modules | ./node_modules | Folder containing all the external modules downloaded and required by the project (only appears while the command `npm install` in the project's root path, is yet launched). |
| Templates | ./views | Folder containing all the code for the pages. |
| Root | ./ | Contains every file for configuring the project, as much as the file **App.tsx**. |

## How to run it ?

In fact, there is different scripts, launchable from npm thanks to the **package.json**. Here is the main list :
- `npm run lint` : check if any linter error is in the repository.
- `npm run lint-fix` : fix any linter error in the repository.
- `npm run start` : launch the app, and opens the browser for showing how to access to the app.